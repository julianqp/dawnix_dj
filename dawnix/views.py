# Create your views here.
from django.contrib.auth.forms import UserCreationForm
import requests
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.template import loader
from django.contrib.auth.models import User
from rest_framework.decorators import api_view

from dawnix.models import Registro, Usuario, Favoritos, Peliculas, RegistroPelicula, ModificarPelicula, Generos
from dawnix.models import ModificarUsuarios
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q

def principal(request):
    """
    View function for home page of site.
    """
    # Render the HTML template index.html
    return render(
        request,
        'principal.html',
    )


def usuarios(request):

    if request.method == 'GET':
        usuario = User.objects.all()
        template = loader.get_template('administrar-usuarios.html')
        context = {
            'usuario': usuario,
            'form': Registro(),
            'form_m': ModificarUsuarios(),
        }
        return HttpResponse(template.render(context, request))

    elif request.method == 'POST':
        form = Registro(request.POST)

        if form.is_valid():
            fav = Favoritos()
            fav.usuario = form.data.get('username')
            fav.save()
            form.save()
            return HttpResponseRedirect('/dawnix/principal/usuarios')
        else:
            html = "<html><head><link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>" \
                   "</head><body><div class='alert alert-danger m-5'>"+str(form.errors.as_data())+"</div></body></html>"
            return HttpResponse(html)


def peliculas(request):
    if request.method == 'GET':
        pelicula = Peliculas.objects.all()
        template = loader.get_template('administrar-peliculas.html')
        context = {
            'pelicula': pelicula,
            'form': RegistroPelicula(),
            'form_m': ModificarPelicula(),
        }
        return HttpResponse(template.render(context, request))

    elif request.method == 'POST':
        form = RegistroPelicula(request.POST)
        if form.is_valid():
            response = requests.get('https://api.themoviedb.org/3/search/movie?api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES&query='+form.data.get('titulo')+'&page=1&include_adult=true');
            contenido = response.json()
            id = contenido['results'][0]['id']
            responsePelicula = requests.get('https://api.themoviedb.org/3/movie/'+str(id)+'?api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES');
            busquedaRealizada = responsePelicula.json();

            # Video
            responseVideo = requests.get('https://api.themoviedb.org/3/movie/'+str(id)+'/videos?api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES');
            video = responseVideo.json();

            # Reparto
            responseReparto = requests.get('https://api.themoviedb.org/3/movie/'+str(id)+'/credits?api_key=831e08f1af6a358dffb3e2b2790c7f3a');
            reparto = responseReparto.json();
            rep = ''
            todos = reparto['cast']
            for i in range(0, 5):
                if i != 4:
                    rep += todos[i]['name'] + ', '
                else:
                    rep += todos[i]['name']
            encontrarDirector = reparto['crew']
            j = 0
            while j < len(encontrarDirector[j]['job']) and encontrarDirector[j]['job'] != 'Director':
                j += 1

            # Categoria
            cate = ''
            numero = 0
            cantidad = len(busquedaRealizada['genres']) - 1

            catego = []

            for elemnt in busquedaRealizada['genres']:
                catego.append(elemnt['name'])
                if numero != cantidad:
                    cate += elemnt['name'] + ', '
                    numero += 1
                else:
                    cate += elemnt['name']

            obj = Peliculas()
            obj.titulo = busquedaRealizada['title']
            obj.URLVideo = 'https://www.youtube.com/embed/'+video['results'][0]['key']
            obj.valoracion = busquedaRealizada['vote_average']
            obj.año = busquedaRealizada['release_date']
            obj.categorias = cate
            obj.descripcion = busquedaRealizada['overview']
            obj.URLPortada = 'https://image.tmdb.org/t/p/original'+busquedaRealizada['poster_path']
            obj.director = encontrarDirector[j]['name']
            obj.reparto = rep

            obj.save()

            # Guardar genero
            for elemnt in catego:
                genero(elemnt, obj.id)

            return HttpResponseRedirect('/dawnix/principal/peliculas')
        else:
            html = "<html><head><link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>" \
                   "</head><body><div class='alert alert-danger m-5'>"+str(form.errors.as_data())+"</div></body></html>"
            return HttpResponse(html)


def genero(genero, id):
    try:
        respestaGenero = Generos.objects.get(genero=genero)
        arrayPeliculas = respestaGenero.peliculas

        if arrayPeliculas == 'null':
            respestaGenero.peliculas = id
        else:
            respestaGenero.peliculas = respestaGenero.peliculas + ',' + str(id)
        respestaGenero.save()
        return

    except ObjectDoesNotExist:
        return


def peliculasGenero(request, genero):

    respuestaPeliculas = Generos.objects.get(genero=genero)
    peliculas = respuestaPeliculas.peliculas

    respuesta = {
        'peliculas': peliculas
    }
    return JsonResponse(respuesta, safe=False)


def usuario(request, pk):
    try:
        if request.method == 'GET':
            response = User.objects.get(id=pk)
            usuario = {
                'id': response.id,
                'username': response.username,
                'first_name': response.first_name,
                'last_name': response.last_name,
                'email': response.email,
            }
            return JsonResponse(usuario)

        elif request.method == 'POST':
            form_m = ModificarUsuarios(request.POST)

            if form_m.is_valid():
                obj = User.objects.get(id=pk)
                if form_m.cleaned_data['email'] != '':
                    obj.email = form_m.cleaned_data['email']
                if form_m.cleaned_data['first_name'] != '':
                    obj.first_name = form_m.cleaned_data['first_name']
                if form_m.cleaned_data['last_name'] != '':
                    obj.last_name = form_m.cleaned_data['last_name']
                obj.save()
                return HttpResponseRedirect('/dawnix/principal/usuarios')
            else:
                html = "<html><head><link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>" \
                   "</head><body><div class='alert alert-danger m-5'>"+str(form_m.errors.as_data())+"</div></body></html>"
                return HttpResponse(html)

    except ObjectDoesNotExist:
        usuario = {
            'error': 'No existe el usuario'
        }

    return JsonResponse(usuario)


def pelicula(request, pk):
    try:
        if request.method == 'GET':
            response = Peliculas.objects.get(id=pk)
            pelicula = {
            'id': response.id,
            'titulo': response.titulo,
            'categorias': response.categorias,
            'descripcion': response.descripcion,
            'año': response.año,
            'director': response.director,
            'reparto': response.reparto,
            'valoracion': response.valoracion,
            'video': response.URLVideo,
            'portada': response.URLPortada,
            }
            return JsonResponse(pelicula)

        elif request.method == 'POST':
            form_m = ModificarPelicula(request.POST)

            if form_m.is_valid():
                obj = Peliculas.objects.get(id=pk)
                if form_m.cleaned_data['titulo'] != '':
                    obj.titulo = form_m.cleaned_data['titulo']
                if form_m.cleaned_data['URLVideo'] != '':
                    obj.URLVideo = form_m.cleaned_data['URLVideo']
                obj.save()
                return HttpResponseRedirect('/dawnix/principal/peliculas')
            else:
                html = "<html><head><link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>" \
                   "</head><body><div class='alert alert-danger m-5'>"+str(form_m.errors.as_data())+"</div></body></html>"
                return HttpResponse(html)

    except ObjectDoesNotExist:
        pelicula = {
            'error': 'No existe la película'
        }

    return JsonResponse(pelicula)


def eliminarUsuario(request, pk):
    usuario = User.objects.get(id=pk)
    favoritos = Favoritos.objects.get(usuario=usuario.username)
    favoritos.delete()
    usuario.delete()
    return HttpResponseRedirect('/dawnix/principal/usuarios')


def eliminarPelicula(request, pk):
    pelicula = Peliculas.objects.get(id=pk)

    # Eliminar pelicula de su categoria correspondiente
    generos = Generos.objects.all()
    for tabla in generos:
        if tabla.peliculas != 'null':
            idPelis = tabla.peliculas.split(',')
            idBuscar = str(pk)

            if idPelis.count(idBuscar) != 0:
                idPelis.remove(idBuscar)
                if len(idPelis) == 0:
                    tabla.peliculas = 'null'
                else:
                    numero = 0
                    cantidad = len(idPelis)-1
                    stro = ''
                    for elem in idPelis:
                        if numero != cantidad:
                            stro += elem + ','
                            numero += 1
                        else:
                            stro += elem
                    tabla.peliculas = stro
                tabla.save()

    #Eliminar pelicula de los favoritos de los usuarios
    favoritos = Favoritos.objects.all()
    for tabla in favoritos:
        if tabla.favoritas != 'null':
            idPelis = tabla.favoritas.split(',')
            idBuscar = str(pk)

            if idPelis.count(idBuscar) != 0:
                idPelis.remove(idBuscar)
                if len(idPelis) == 0:
                    tabla.favoritas = 'null'
                else:
                    numero = 0
                    cantidad = len(idPelis)-1
                    stro = ''
                    for elem in idPelis:
                        if numero != cantidad:
                            stro += elem + ','
                            numero += 1
                        else:
                            stro += elem
                    tabla.favoritas = stro
                tabla.save()
    pelicula.delete()
    return HttpResponseRedirect('/dawnix/principal/peliculas')


def busqueda(request, titulo):
    array = []
    try:
        if request.method == 'GET':
            response = Peliculas.objects.filter(Q(titulo__icontains=titulo))

            for pelicula in response:
                array.append(pelicula.id)

            return JsonResponse(array, safe=False)

    except ObjectDoesNotExist:
        return JsonResponse(array, safe=False)


def peliculasFavoritas(request, usuario):
    respuestaPeliculas = Favoritos.objects.get(usuario=usuario)
    peliculas = respuestaPeliculas.favoritas

    respuesta = {
        'peliculas': peliculas
    }
    return JsonResponse(respuesta, safe=False)


def insertarFavoritas(request, usuario, pk):
    try:
        favoritas = Favoritos.objects.get(usuario=usuario)

        if favoritas.favoritas == 'null':
            favoritas.favoritas = pk
        else:
            favoritas.favoritas = favoritas.favoritas + ',' + str(pk)

        favoritas.save()
        return JsonResponse({})

    except ObjectDoesNotExist:
        return