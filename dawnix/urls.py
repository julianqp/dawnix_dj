from django.conf.urls import url
from django.urls import path, include
from . import views

urlpatterns = [
 path('', views.principal, name='principal'),
 path('usuarios', views.usuarios, name='usuarios'),
 path('peliculas', views.peliculas, name='peliculas'),
 path('usuarios/<int:pk>', views.usuario),
 path('usuarios/eliminar/<int:pk>', views.eliminarUsuario),
 path('peliculas/eliminar/<int:pk>', views.eliminarPelicula),
 path('peliculas/<int:pk>', views.pelicula),
 path('peliculas/buscar/<titulo>', views.busqueda),
 path('peliculas/<genero>', views.peliculasGenero),
 path('peliculas/favoritas/<usuario>', views.peliculasFavoritas),
 path('peliculas/favoritas/<usuario>/insertar/<int:pk>', views.insertarFavoritas),
 path('usuarios/edit', views.Usuario, name='editar'),

]
