import uuid
from decimal import Decimal

from django.db import models
from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import *
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.compat import MinValueValidator, MaxValueValidator


class Peliculas(models.Model):
    titulo = models.CharField(max_length=100, null=True)
    categorias = models.TextField(null=True, blank=True)
    descripcion = models.TextField(null=True, blank=True)
    año = models.DateField(null=True, blank=True)
    director = models.CharField(max_length=100, null=True, blank=True)
    reparto = models.TextField(null=True, blank=True)
    valoracion = models.DecimalField(max_digits=3, decimal_places=1,
                                     validators=[MinValueValidator(0), MaxValueValidator(10)], null=True, blank=True)
    URLVideo = models.URLField(null=True)
    URLPortada = models.URLField(null=True, blank=True)


    def __str__(self):
        return self.titulo


class Registro(UserCreationForm):
    first_name = forms.CharField(max_length=140, required=True)
    last_name = forms.CharField(max_length=140, required=False)
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2',
        )


class Usuario(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'last_name',
            'email',
        )


class ModificarUsuarios(ModelForm):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
        )


class ModificarPelicula(ModelForm):
    class Meta:
        model = Peliculas
        fields = (
            'titulo',
            'URLVideo',

        )


class RegistroPelicula(ModelForm):
    class Meta:
        model = Peliculas
        fields = (
            'titulo',
            'categorias',
            'descripcion',
            'año',
            'director',
            'reparto',
            'valoracion',
            'URLPortada',
            'URLVideo',
        )


class Generos(models.Model):
    genero = models.CharField(max_length=200)
    peliculas = models.TextField(null=True, default='null')

    def __str__(self):
        return self.genero


class Favoritos(models.Model):
    usuario = models.CharField(max_length=100)
    favoritas = models.TextField(null=True, default='null')

    def __str__(self):
        return self.usuario
