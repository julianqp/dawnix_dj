
function vaciar(idpadre) {
    var array_nodos = document.getElementById(idpadre).childNodes;
    console.log(array_nodos)
    for (var i = 0; i < array_nodos.length; i++) {
        if (array_nodos[i].nodeType == 1) {
             console.log('entro')
            array_nodos[i].parentNode.removeChild(array_nodos[i]);
        }
    }
    console.log('No deberia tener valor ',array_nodos)
};


function myFunction(user) {
    console.log(user);
    $.ajax({
        method: 'GET',
        url: 'http://127.0.0.1:8000/dawnix/principal/usuarios/' + user,
        success: function (respuesta) {
            vaciar('editar');
            salida = '<div>' +
                '<label class="form-control separador mb-2" name="id" placeholder="">' + respuesta.id + '</label>' +
                '<input class="form-control separador mb-2" type="text" name="username" value="' + respuesta.username + '" readonly>' +
                '<input class="form-control separador mb-2" type="text" name="first_name" placeholder="' + respuesta.first_name + '">' +
                '<input class="form-control separador mb-2" type="text" name="last_name" placeholder="' + respuesta.last_name + '">' +
                '<input class="form-control separador mb-3" type="text" name="email" placeholder="' + respuesta.email + '">' +
                '<input class="btn btn-block btn-outline-dark" type="submit" value="Guardar">' +
                '</div>'
            $('#modaleditar').attr('action', 'usuarios/'+respuesta.id)
            $('#editar').append(salida);
        },
        error: function (par) {
            alert("Datos no encontrados");
        }
    });
}

function paseUsuario(user) {
    vaciar('botones')
    salida = "<div>"+
        "<button type='submit' class='btn btn-block btn-outline-dark'>Eliminar</button>" +
        "<button class='btn btn-block btn-outline-danger cancelar' data-dismiss='modal' type='submit'>Cancelar</button>"+
        "</div>"

    $('#botonEliminar').attr('action', 'usuarios/eliminar/'+user)
    $('#botones').append(salida)
}

function myFunction(user) {
    console.log(user);
    $.ajax({
        method: 'GET',
        url: 'http://127.0.0.1:8000/dawnix/principal/usuarios/' + user,
        success: function (respuesta) {
            vaciar('editar');
            salida = '<div>' +
                '<label class="form-control separador mb-2" name="id" placeholder="">' + respuesta.id + '</label>' +
                '<input class="form-control separador mb-2" type="text" name="username" value="' + respuesta.username + '" readonly>' +
                '<input class="form-control separador mb-2" type="text" name="first_name" placeholder="' + respuesta.first_name + '">' +
                '<input class="form-control separador mb-2" type="text" name="last_name" placeholder="' + respuesta.last_name + '">' +
                '<input class="form-control separador mb-3" type="text" name="email" placeholder="' + respuesta.email + '">' +
                '<input class="btn btn-block btn-outline-dark" type="submit" value="Guardar">' +
                '</div>'
            $('#modaleditar').attr('action', 'usuarios/'+respuesta.id)
            $('#editar').append(salida);
        },
        error: function (par) {
            alert("Datos no encontrados");
        }
    });
}


// Buscador en tiempo real
(function ($) {
    $('#filtrar').keyup(function () {
        var rex = new RegExp($(this).val(), 'i');
        $('.buscar tr').hide();
        $('.buscar tr').filter(function () {
            return rex.test($(this).find('.atributo-usuario').text());
        }).show();
    })
}(jQuery));
