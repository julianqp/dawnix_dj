
// Vacía la modal
function vaciar(idpadre) {
    var array_nodos = document.getElementById(idpadre).childNodes;
    for (var i = 0; i < array_nodos.length; i++) {
        if (array_nodos[i].nodeType == 1) {
            array_nodos[i].parentNode.removeChild(array_nodos[i]);
        }
    }
};

// Completar la modal de eliminar películas
function pasePeliculas(id) {
    vaciar('botonesPelicula')
    salida = "<div>"+
        "<button type='submit' class='btn btn-block btn-outline-dark'>Eliminar</button>" +
        "<button class='btn btn-block btn-outline-danger cancelar' data-dismiss='modal' type='submit'>Cancelar</button>"+
        "</div>"

    $('#botonEliminar').attr('action', 'peliculas/eliminar/'+id)
    $('#botonesPelicula').append(salida)
}


// Buscador en tiempo real
(function ($) {
    $('#filtrar').keyup(function () {
        var rex = new RegExp($(this).val(), 'i');
        $('.buscar tr').hide();
        $('.buscar tr').filter(function () {
            return rex.test($(this).find('.atributo-titulo').text());
        }).show();
    })
}(jQuery));
