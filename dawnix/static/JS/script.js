
$(document).ready(function() {
    // "https://api.themoviedb.org/3/list/107803?api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES"
    let peliculas = [{
        tipo: 'peliculas-accion',
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/Aventura'
    },{
        tipo: 'peliculas-comedia',
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/Comedia'
    },{
        tipo: 'peliculas-familia',
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/Familia'
    },{
        tipo: 'peliculas-drama',
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/Drama'
    },]

    peliculas.forEach(pelicula => {
        $.ajax({
            method: "GET",
            url: pelicula.url,
            success: function (par) {
                let contenido = ''
                arrayPeliculas = par.peliculas.split(',')
                var longitud = arrayPeliculas.length
                $.each(arrayPeliculas, function (i, item) {
                    $.ajax({
                            method: "GET",
                            url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/' + item,
                            async: false,
                            success: function (par) {
                                if ((i % 4) === 0) {
                                    if (i !== 0) {
                                        contenido += '<div class="carousel-item margenes">' +
                                            '<div class="row">';
                                    } else {contenido += '<div class="carousel-item margenes active">' +
                                            '<div class="row">';
                                    }
                                }
                                contenido += '<div class="col-md-3">' +
                                    '<img style="max-width:99%;" onclick="descripcion(\'' + par.titulo.replace(/["']/g, "") + '\',\'' + par.descripcion.replace(/["']/g, "") + '\',\'' + par.portada + '\',\'' + par.año + '\',\'' + par.valoracion + '\',\'' + par.portada + '\',\'' + par.video + '\',\'' + par.director + '\',\'' + par.reparto + '\',\'' + par.id + '\');" type="button" data-toggle="modal" data-target="#exampleModalCenter" src="' + par.portada + '" alt="' + par.titulo + '">' +
                                    '</div>';

                                if ((i % 4) === 3 || i === longitud - 1) {
                                    contenido += '</div>' +
                                        '</div>';
                                }
                            }
                        }
                    );
                });
                var resultado = '<div class="row blog">' +
                                '<div class="col-md-12">' +
                                '<div id="carousel' + pelicula.tipo + '" class="carousel slide blog" data-ride="carousel">' +
                                '<div class="carousel-inner">' +
                                contenido +
                                '</div>' +
                                '<a class="carousel-control-prev flecha" href="#carousel' + pelicula.tipo + '" role="button" data-slide="prev">' +
                                '<span class="carousel-control-prev-icon" aria-hidden="true"></span>' +
                                '<span class="sr-only">Previous</span>' +
                                '</a>' +
                                '<a class="carousel-control-next flecha" href="#carousel' + pelicula.tipo + '" role="button" data-slide="next">' +
                                '<span class="carousel-control-next-icon" aria-hidden="true"></span>' +
                                '<span class="sr-only">Next</span>' +
                                '</a>' +
                                '</div>' +
                                '</div>' +
                                '</div>'
                $('#' + pelicula.tipo).append(resultado);
            }
        });
    });
});


$('.carousel').carousel('pause')


$("#busqueda").on('click', function() {
    let titulo = document.getElementById("contenido").value;
    let url = "http://127.0.0.1:8000/dawnix/principal/peliculas/buscar/" + titulo
    $.ajax({
        method: "GET",
        url: url,
        success: function (par) {
            if (par.length != 0)
            {
                let contenido = ''
                peticionSinCarrusel(par, "Resultado: " + titulo, function (res) {
                    resultado = res
                    vaciar('busquedas');
                    $('#busquedas').append(resultado);
                });
            }else {
                let resultado = '<section class="carousel-populares">' +
                                '<div class="clasificacion">' +
                                '<h1>Resultado: '+titulo+'</h1>' +
                                '<h2>No hay resultados</h2></div></section>';
                 vaciar('busquedas');
                 $('#busquedas').append(resultado);
            }
        }
    });
});


$("#contenido").keypress(function(event) {
    if (event.which == 13) {
        let titulo = document.getElementById("contenido").value;
        let url = "http://127.0.0.1:8000/dawnix/principal/peliculas/buscar/" + titulo
        $.ajax({
            method: "GET",
            url: url,
            success: function (par) {
                if (par.length != 0)
                {
                    let contenido = ''
                    peticionSinCarrusel(par, "Resultado: " + titulo, function (res) {
                        resultado = res
                        vaciar('busquedas');
                        $('#busquedas').append(resultado);
                    });
                }else {
                    let resultado = '<section class="carousel-populares">' +
                                    '<div class="clasificacion">' +
                                    '<h1>Resultado: '+titulo+'</h1>' +
                                    '<h2>No hay resultados</h2></div></section>';
                     vaciar('busquedas');
                     $('#busquedas').append(resultado);
                }
            }
        });
    }
});


function vaciar(idpadre) {
    var array_nodos = document.getElementById(idpadre).childNodes;
    for (var i = 0; i < array_nodos.length; i++) {
        if (array_nodos[i].nodeType == 1) {
            array_nodos[i].parentNode.removeChild(array_nodos[i]);
        }
    }
};


$("#favoritas").on('click', function() {
    $.ajax({
        method: "GET",
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/favoritas/'+ document.getElementById('guardadoid').textContent,
        success: function (par) {
            let contenido = ''
            arrayPeliculas = par.peliculas.split(',')
            peticionSinCarrusel(arrayPeliculas, "Favoritas", function(res) {
                resultado = res
                vaciar('busquedas');
                $('#busquedas').append(resultado);
            });
        }
    });
});


$("#Accion").on('click', function() {
     $.ajax({
        method: "GET",
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/Acción',
        success: function (par) {
            let contenido = ''
            arrayPeliculas = par.peliculas.split(',')
            peticionSinCarrusel(arrayPeliculas, "Acción", function(res) {
                resultado = res
                vaciar('busquedas');
                $('#busquedas').append(resultado);
            });
        }
    });
});


$("#Drama").on('click', function() {
     $.ajax({
        method: "GET",
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/Drama',
        success: function (par) {
            let contenido = ''
            arrayPeliculas = par.peliculas.split(',')
            peticionSinCarrusel(arrayPeliculas, "Drama", function(res) {
                resultado = res
                vaciar('busquedas');
                $('#busquedas').append(resultado);
            });
        }
    });
});


$("#Ficcion").on('click', function() {
     $.ajax({
        method: "GET",
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/Ciencia%20ficción',
        success: function (par) {
            let contenido = ''
            arrayPeliculas = par.peliculas.split(',')
            peticionSinCarrusel(arrayPeliculas, "Ciencia Ficción", function(res) {
                resultado = res
                vaciar('busquedas');
                $('#busquedas').append(resultado);
            });
        }
    });
});


$("#Comedia").on('click', function() {
    $.ajax({
        method: "GET",
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/Comedia',
        success: function (par) {
            let contenido = ''
            arrayPeliculas = par.peliculas.split(',')
            peticionSinCarrusel(arrayPeliculas, "Comedia", function(res) {
                resultado = res
                vaciar('busquedas');
                $('#busquedas').append(resultado);
            });
        }
    });
});


$("#Aventura").on('click', function() {
     $.ajax({
        method: "GET",
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/Aventura',
        success: function (par) {
            let contenido = ''
            arrayPeliculas = par.peliculas.split(',')
            peticionSinCarrusel(arrayPeliculas, "Aventura", function(res) {
                resultado = res
                vaciar('busquedas');
                $('#busquedas').append(resultado);
            });
        }
    });
});


$("#Animation").on('click', function() {
     $.ajax({
        method: "GET",
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/Animación',
        success: function (par) {
            let contenido = ''
            arrayPeliculas = par.peliculas.split(',')
            peticionSinCarrusel(arrayPeliculas, "Animación", function(res) {
                resultado = res
                vaciar('busquedas');
                $('#busquedas').append(resultado);
            });
        }
    });
});


$("#Familiar").on('click', function() {
     $.ajax({
        method: "GET",
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/Familia',
        success: function (par) {
            let contenido = ''
            arrayPeliculas = par.peliculas.split(',')
            peticionSinCarrusel(arrayPeliculas, "Familiar", function(res) {
                resultado = res
                vaciar('busquedas');
                $('#busquedas').append(resultado);
            });
        }
    });
});


function descripcion(titulo, descripcion, foto, fecha, votacion, foto, enlace, director, reparto, id) {
    vaciar('exampleModalCenter');
    let color = ''
    if (votacion >= 7) {
        color = 'text-success'
    } else if (votacion < 5) {
        color = 'text-danger'
    } else if (votacion >= 5) {
        color = 'text-warning'
    }
    let frame = enlace !== "No hay datos" ? '<iframe class="mx-auto center-block" width="460" height="255" src="' + enlace + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' : '<p class="card-text"><small class="text-muted">Video no disponible</small></p>';
    let imagen = foto !== null ? foto : ''
    const resultado =
        '<div class="modal-dialog modal-lg" role="document">' +
        '<div class="modal-content">' +
        '<div class="modal-header degradado">' +
        '<h5 class="modal-title" id="exampleModalCenterTitle" style="color: white">' + titulo + '</h5>' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button>' +
        '</div>' +
        '<div class="modal-body" style="color: black; border:none;">' +
        '<div class="card mb-3">' +
        '<div class="row no-gutters">' +
        '<div class="col-md-4">' +
        '<a target=”_blank" href="' + imagen + '"><img src="' + imagen + '" class=" m-2 card-img" alt="' + titulo + '"> </a>' +
        '</div>' +
        '<div class="col-md-8">' +
        '<div class="card-body">' +
        '<p class="card-text">' + descripcion + '</p>' +
        '<p class="card-text"><small class="text-muted">Director: '+director+'</small></p>' +
        '<p class="card-text"><small class="text-muted">Reparto: '+ reparto +'</small></p>' +
        '<p class="card-text"><small class="text-muted"><span class=' + color + '>Valoracion:  ' + votacion + '</span></small></p>' +
        '<p class="card-text"><small class="text-muted">Fecha de lanzamiento: ' + fecha + '</small></p>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="mt-2 mx-auto">' + frame + '</div>' +
        '</div>' +
        '</div>' +
        '<div class="modal-footer" style="padding:0rem 1rem 1rem 1rem; border:none;">' +
        '<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Atrás</button>' +
        '<button class="btn btn-outline-warning text-dark" data-dismiss="modal" type="submit" onclick="insertarFavorito(\''+ document.getElementById('guardadoid').textContent + '\','+id+')">Añadir a favoritos</button>' +
        '</div>' +
        '</div>' +
        '</div>'
    $('#exampleModalCenter').append(resultado);
};


function peticionSinCarrusel(ids, entrada, callback) {
    var contenido = ''
    var resultado = ''

    var longitud = ids.length
    $.each(ids, function(i, item) {
        $.ajax({
            method: "GET",
            url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/' + item,
            async: false,
            success: function (par) {
                if ((i % 4) === 0) {
                    if (i !== 0) {
                        contenido += '<div class="margenes">' +
                            '<div class="row">';
                    } else {
                        contenido += '<div class="margenes">' +
                            '<div class="row">';
                    }
                }
                contenido += '<div class="col-md-3">' +
                    '<img style="max-width:99%; margin-bottom: 10%" onclick="descripcion(\'' + par.titulo.replace(/["']/g, "") + '\',\'' + par.descripcion.replace(/["']/g, "") + '\',\'' + par.portada + '\',\'' + par.año + '\',\'' + par.valoracion + '\',\'' + par.portada + '\',\'' + par.video + '\',\'' + par.director + '\',\'' + par.reparto + '\',\'' + par.id + '\');" type="button" data-toggle="modal" data-target="#exampleModalCenter" src="' + par.portada + '" alt="' + par.titulo + '">' +
                    '</div>';

                if ((i % 4) === 3 || i === longitud - 1) {
                    contenido += '</div>' +
                        '</div>';
                }
            }
        });
    });

    resultado =
                '<section class="carousel-populares">' +
                '<div class="clasificacion">' +
                '<h1>' + entrada + '</h1>' +
                '</div>' +
                '<div class="row blog">' +
                '<div class="col-md-12">' +
                '<div id="sincarousel' + entrada + '">' +
                '<div>' +
                contenido +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</section>'
    callback(resultado)
}

function insertarFavorito(usuario, id) {
    $.ajax({
        method: 'GET',
        url: 'http://127.0.0.1:8000/dawnix/principal/peliculas/favoritas/' + usuario + '/insertar/' + id,
        success: function () {
        }
    });
}
