# Generated by Django 2.1.7 on 2019-05-19 13:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dawnix', '0003_auto_20190519_1239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='peliculas',
            name='titulo',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
