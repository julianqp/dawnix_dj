# Generated by Django 2.1.7 on 2019-05-19 13:16

from django.db import migrations, models
import rest_framework.compat


class Migration(migrations.Migration):

    dependencies = [
        ('dawnix', '0004_auto_20190519_1315'),
    ]

    operations = [
        migrations.AlterField(
            model_name='peliculas',
            name='URLVideo',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='peliculas',
            name='año',
            field=models.PositiveIntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='peliculas',
            name='categorias',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='peliculas',
            name='descripcion',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='peliculas',
            name='director',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='peliculas',
            name='reparto',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='peliculas',
            name='valoracion',
            field=models.DecimalField(blank=True, decimal_places=1, max_digits=3, validators=[rest_framework.compat.MinValueValidator(0), rest_framework.compat.MaxValueValidator(10)]),
        ),
    ]
