from django.apps import AppConfig


class DawnixConfig(AppConfig):
    name = 'dawnix'
