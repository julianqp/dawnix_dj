from django.contrib import admin

from .models import Peliculas, Generos, Favoritos

admin.site.register(Peliculas),
admin.site.register(Generos),
admin.site.register(Favoritos)
