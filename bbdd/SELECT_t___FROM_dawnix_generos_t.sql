INSERT INTO dawnix_generos (id, genero, peliculas) VALUES (1, 'Acción', '1,2,3,4,5,7,8,9,13,15,17,18,20,21,31,32,33,34,35,36,37,38,41,47,48,49');
INSERT INTO dawnix_generos (id, genero, peliculas) VALUES (2, 'Aventura', '1,2,4,5,6,7,8,9,13,14,15,17,21,26,27,28,29,30,31,32,33,34,35,36,42,47,48,49,50');
INSERT INTO dawnix_generos (id, genero, peliculas) VALUES (3, 'Animación', '2,12,17,22,23,24,25,26,27,28,29,30');
INSERT INTO dawnix_generos (id, genero, peliculas) VALUES (4, 'Comedia', '2,8,11,12,13,16,20,22,23,24,25,27,28,29,30,33,34,50');
INSERT INTO dawnix_generos (id, genero, peliculas) VALUES (5, 'Ciencia ficción', '2,3,4,7,8,15,21,31,32,33,34,35,36,37,41,42,46,47,48,49');
INSERT INTO dawnix_generos (id, genero, peliculas) VALUES (6, 'Drama', '10,11,19,37,38,39,40,41,42,43,44,45,46');
INSERT INTO dawnix_generos (id, genero, peliculas) VALUES (7, 'Familia', '8,12,16,17,22,23,24,25,26,27,28,29,30,33,50');